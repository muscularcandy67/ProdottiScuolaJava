public class ProdottiNonAlimentari extends Prodotti{
    private String materiale;

    public ProdottiNonAlimentari(String codice, double prezzo, String materiale) {
        super(codice, prezzo);
        setMateriale(materiale);
    }

    public boolean setMateriale(String materiale){
        if(materiale!=null){
            if (materiale.matches("[a-zA-Z]*")){
                this.materiale=materiale;
                return true;
            }
        }
        return false;
    }

    public boolean applicaSconto(){
        if(materiale.equalsIgnoreCase("plastica") || materiale.equalsIgnoreCase("carta") || materiale.equalsIgnoreCase("vetro")){
            setPrezzo(getPrezzo()-(getPrezzo()*0.1));
            return true;
        }
        return false;
    }
}
