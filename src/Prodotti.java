public class Prodotti {
    private String codice;
    private double prezzo;

    public Prodotti(String codice, double prezzo) {
        setCodice(codice);
        setPrezzo(prezzo);
    }

    public String getCodice() {
        return codice;
    }

    public boolean setCodice(String codice) {
        if(codice.matches("[0-9]+")){
            this.codice=codice;
            return true;
        }
        return false;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public boolean setPrezzo(double prezzo) {
        if(prezzo>=0){
            this.prezzo=prezzo;
            return true;
        }
        return false;
    }

    public void applicaSconto(){
        prezzo-= (prezzo*0.05);
    }
}
