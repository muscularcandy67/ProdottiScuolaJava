import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.StringTokenizer;

public class ProdottiAlimentari extends Prodotti {

    private LocalDate dataDiScadenza;

    public ProdottiAlimentari(String codice, double prezzo, String dataDiScadenza) {
        super(codice, prezzo);
        setDataDiScadenza(dataDiScadenza);
    }

    public ProdottiAlimentari(String codice, double prezzo, LocalDate dataDiScadenza) {
        super(codice, prezzo);
        setDataDiScadenza(dataDiScadenza);
    }


    public boolean setDataDiScadenza(String dataDiScadenza) {
        if(dataDiScadenza.matches("[0-9/]+")){
            StringTokenizer s = new StringTokenizer(dataDiScadenza, "/");
            this.dataDiScadenza =  LocalDate.of(Integer.valueOf(s.nextToken()),Integer.valueOf(s.nextToken()),Integer.valueOf(s.nextToken()));
            return true;
        }
        return false;
    }

    public boolean setDataDiScadenza(LocalDate dataDiScadenza) {
        if (dataDiScadenza != null) {
            this.dataDiScadenza = dataDiScadenza;
            return true;
        }
        return false;
    }

    public LocalDate getDataDiScadenza() {
        return dataDiScadenza;
    }

    public double applicaSconto() {
        if ((int)ChronoUnit.DAYS.between(dataDiScadenza, LocalDate.now())<=10) {
            return getPrezzo() - (getPrezzo()*0.2);
        } else return getPrezzo() - (getPrezzo() *0.1);
    }
}
